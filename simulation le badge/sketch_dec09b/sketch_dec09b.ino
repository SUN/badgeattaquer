
#include <OneWireHub.h>
#include <DS2401.h>
#include <OneWireHub_config.h>
#include <OneWireItem.h>
#include <platform.h>




#include "DS1990.h" 

constexpr uint8_t pin_onewire   { 8 };
auto hub     = OneWireHub(pin_onewire);

bool blinking(void);
int add_a=0x00,add_b=0x00,add_c=0x00,add_d; 

static int flag_ab,flag_bc,flag_cd;

static int add_D(){
  add_d=0;
  int count;
  Serial.println("********************************************************************add_d");
  for(count =1;count<=255;count++){
    
      add_d++;
     
     Serial.println(add_d,HEX);
    }
    if(count+1 == 256 )
    flag_cd = 1;
    
    return add_d; 
    
  }

/*
static int add_C(){
   add_c=0;
   int count;
   if(flag_cd == 1){
   add_c++;
   flag_cd = 0;}
   Serial.println("********************************************************************add_c");
  for(count =1;count<=255;count++){
    if(count<16){
      Serial.print("0");
      }
      add_c++;
      
  
 //     Serial.println(add_c,HEX);
    }
    if(count+1 == 256 )
    flag_bc = 1;
    return add_c; 
  }


static int add_B(){
   add_b=0;
   int count;
   if(flag_bc == 1){
   add_b++;
   flag_bc = 0;}
   Serial.println("**********************************************************************add_b");
  for(count =1;count<=255;count++){
    if(count<16){
      Serial.print("0");
      }
      add_b++;
   //   Serial.println(add_b,HEX);
    }
    if(count+1 == 256 )
    flag_ab = 1;
    return add_b; 
  }


static int add_A(){
   add_a=0;
   int count;
   if(flag_ab == 1){
   add_a++;
   flag_ab = 0;}
   Serial.println("***************************************************add_a");
  for(count =1;count<=255;count++){
    if(count<16){
      Serial.print("0");
      }
      add_a++;
 //    Serial.println(add_a,HEX);
   
    }
    return add_a; 
  }

*/


void setup()
{
    Serial.begin(9600);

}

void loop()
{
  
   auto ds1990A = DS1990( DS1990::family_code,  0x00,0x00,add_a, add_b, add_c, add_d); 
    Serial.println("OneWire-Hub DS1990 Serial Number used as iButton");

     Serial.println(add_d,HEX);
    hub.attach(ds1990A);
    Serial.println("config done");
    hub.poll();
   delay(1000);
  
}
     
