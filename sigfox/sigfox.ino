
#include <SigFox.h>

#include <ArduinoLowPower.h>

#include <RTCZero.h>

typedef struct __attribute__ ((packed)) sigfox_message { // see http://www.catb.org/esr/structure-packing/#_structure_alignment_and_padding
  byte addr0[6];
   byte addr1[6];
} SigfoxMessage;
 
// stub for message which will be sent
SigfoxMessage msg;

// =================== UTILITIES ===================
void reboot() {
  NVIC_SystemReset();
  while (1);
}
// =================================================


void setup() {
  Serial.begin(115200);
  while (!Serial);

  if (!SigFox.begin()) {
    Serial.println("SigFox error, rebooting");
    reboot();
  }

  delay(100); // Wait at least 30ms after first configuration

  // Enable debug prints and LED indication
  SigFox.debug();

  // Read and convert the module temperature
  
  byte addr0[6] ={0x02,0x39,0xD7,0xB3,0x18,0x3F};
  byte addr1[6]= {0x01,0x39,0xD7,0xB3,0x18,0x3F};
  Serial.print(msg.addr0[1], HEX); // display what we will send in Hexadecimal
  Serial.print(" (");
  Serial.print(msg.addr0[2], HEX); //
   Serial.print(msg.addr1[5], HEX); 

  // Clears all pending interrupts
  SigFox.status();
  delay(1);

  // Send the data
  SigFox.beginPacket();
  SigFox.write((uint8_t*)&msg, sizeof(SigfoxMessage));
  
  Serial.print("Status: ");
  Serial.println(SigFox.endPacket());

  SigFox.end();
  
}

void loop() {
  delay(10000);

  }
