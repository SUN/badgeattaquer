#include <SigFox.h>
#include <ArduinoLowPower.h>

void setup() {
  Serial.begin(115200);
  while (!Serial) {};

  if (!SigFox.begin()) {
    Serial.println("Shield error or not present!");
    return;
}

void loop(){
  SigFox.begin();
  SigFox.beginPacket();
  SigFox.print("123456789012");
  SigFox.endPacket();
  while(1);
}
